<div class="widget-body">
    <fieldset>
        <legend>
            Form data for driver info
        </legend>
        <div class="form-group">
            <label>Driver info title</label>
            <input type="text" class="form-control" name="contents[driver_info][title]" value="{{ $page->contents['driver_info']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Driver info subtitle</label>
            <input type="text" class="form-control" name="contents[driver_info][subtitle]" value="{{ $page->contents['driver_info']['subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Driver info details</label>
            <textarea rows="5" class="form-control editor" name="contents[driver_info][details]" required>{{ $page->contents['driver_info']['details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>Driver info details URL</label>
            <input type="text" class="form-control" name="contents[driver_info][url]" value="{{ $page->contents['driver_info']['url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Driver info image (540X640)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['driver_info']['image'])){{ '/storage/' .$page->contents['driver_info']['image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="driver info image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="driver_info_image" @if(!isset($page->contents['driver_info']['image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for user info
        </legend>
        <div class="form-group">
            <label>User info title</label>
            <input type="text" class="form-control" name="contents[user_info][title]" value="{{ $page->contents['user_info']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>User info subtitle</label>
            <input type="text" class="form-control" name="contents[user_info][subtitle]" value="{{ $page->contents['user_info']['subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>User info details</label>
            <textarea rows="5" class="form-control editor" name="contents[user_info][details]" required>{{ $page->contents['user_info']['details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>User info URL</label>
            <input type="text" class="form-control" name="contents[user_info][url]" value="{{ $page->contents['user_info']['url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>User info Image (540X465)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['user_info']['image'])){{ '/storage/' .$page->contents['user_info']['image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="user info image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="user_info_image" @if(!isset($page->contents['user_info']['image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for coverage area
        </legend>
        <div class="form-group">
            <label>Coverage area title</label>
            <input type="text" class="form-control" name="contents[coverage_area][title]" value="{{ $page->contents['coverage_area']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Coverage area URL</label>
            <input type="text" class="form-control" name="contents[coverage_area][url]" value="{{ $page->contents['coverage_area']['url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Coverage area image (1111X738)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['coverage_area']['image'])){{ '/storage/' .$page->contents['coverage_area']['image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="coverage area image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="coverage_area_image" @if(!isset($page->contents['coverage_area']['image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

    @include('admin.template.partials.form_submit')
</div>



