
<div class="widget-body">
    <fieldset>
        <legend>
            Contact Us Page
        </legend>
        <div class="form-group">
            <label>Location Title</label>
            <input type="text" class="form-control" name="contents[location_title]" value="{{ $page->contents['location_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Location Details</label>
            <textarea class="form-control" name="contents[location_details]" rows="5">{!! $page->contents['location_details'] ?? '' !!}</textarea>
        </div>
        <div class="form-group">
            <label>Visit Hours Title</label>
            <input type="text" class="form-control" name="contents[visit_hour_title]" value="{{ $page->contents['visit_hour_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Visit Hours Details</label>
            <textarea class="form-control editor" name="contents[visit_hour_details]" rows="8">{!! $page->contents['visit_hour_details'] ?? '' !!}</textarea>
        </div>
        <div class="form-group">
            <label>Connect Us Title</label>
            <input type="text" class="form-control" name="contents[connect_us_title]" value="{{ $page->contents['connect_us_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Connect Us Details</label>
            <textarea class="form-control" name="contents[connect_us_details]" rows="5">{!! $page->contents['connect_us_details'] ?? '' !!}</textarea>
        </div>
    </fieldset>
    @include('admin.template.partials.form_submit')

</div>





