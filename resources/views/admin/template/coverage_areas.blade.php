
<div class="widget-body">
    <fieldset>
        <legend>
            Form data for coverage areas
        </legend>

        <div class="form-group">
            <label>Coverage area title</label>
            <input type="text" class="form-control" name="contents[coverage_areas_title]" value="{{ $page->contents['coverage_areas_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Coverage area subtitle</label>
            <input type="text" class="form-control" name="contents[coverage_areas_subtitle]" value="{{ $page->contents['coverage_areas_subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Coverage area details</label>
            <textarea rows="5" class="form-control editor" name="contents[coverage_areas_details]" required>{{ $page->contents['coverage_areas_details'] ?? '' }}</textarea>
        </div>

        <legend>
            Form data for coming soon
        </legend>
        <div class="form-group">
            <label>Coming soon title</label>
            <input type="text" class="form-control" name="contents[coming_soon_title]" value="{{ $page->contents['coming_soon_title'] ?? '' }}" />
        </div>
        <div class="form-group">
            <label>Coming soon subtitle</label>
            <input type="text" class="form-control" name="contents[coming_soon_subtitle]" value="{{ $page->contents['coming_soon_subtitle'] ?? '' }}" />
        </div>
        <div class="form-group">
            <label>Coming soon details</label>
            <textarea rows="5" class="form-control editor" name="contents[coming_soon_details]">{{ $page->contents['coming_soon_details'] ?? '' }}</textarea>
        </div>
    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


