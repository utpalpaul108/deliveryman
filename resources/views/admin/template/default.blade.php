
<div class="widget-body">
    <fieldset>
        <legend>
            Form Elements
        </legend>

        <div class="form-group">
            <label>Page content</label>
            <textarea class="form-control editor" name="contents[page_content]" rows="8">{!! $page->contents['page_content'] ?? '' !!}</textarea>
        </div>
    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


