@extends('admin.layout.app')

@section('page_title','Admin | Create driver')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DriverController@index') }}">All drivers</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Create driver</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" action="{{ action('Admin\DriverController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">
                            @include('flash::message')
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Crete Driver </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Driver Data
                                            </legend>
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" name="password" placeholder="XXXXXXXXXX" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" class="form-control" name="password_confirmation" placeholder="XXXXXXXXXX" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Phone No</label>
                                                <input type="text" class="form-control" name="phone_no" value="{{ old('phone_no') }}" placeholder="Phone No" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Address</label>
                                                <textarea rows="5" class="form-control" name="address" required >{{ old('address') }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Schedule From</label>
                                                <div class="input-group">
                                                    <input class="form-control clockpicker" type="text" name="working_schedule_from" value="{{ old('working_schedule_from') }}" placeholder="Schedule From" data-autoclose="true">
                                                    <span class="input-group-append"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Schedule To</label>
                                                <div class="input-group">
                                                    <input class="form-control clockpicker" type="text" name="working_schedule_to" value="{{ old('working_schedule_to') }}" placeholder="Schedule To" data-autoclose="true">
                                                    <span class="input-group-append"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Create
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Profile Image(130 X 130):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="http://placehold.it/200x200" width="100%" alt="header image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="profile_image" required>
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            /*
			 * CLOCKPICKER
			 */

            $('.clockpicker').clockpicker({
                placement: 'top',
                donetext: 'Done'
            });

            // For Multiple Select
            if($.fn.select2) {
                $("select.select2").each(function(){var e=$(this),t=e.attr("data-select-width")||"100%";e.select2({allowClear:!0,width:t}),e=null});
            }
            if ($.fn.select2) {
                $("select.select2").each(function () {
                    var e = $(this), t = e.attr("data-select-width") || "100%";
                    e.select2({allowClear: !0, width: t}), e = null
                });
            }

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

            $('div.alert').delay(3000).fadeOut(350);

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
