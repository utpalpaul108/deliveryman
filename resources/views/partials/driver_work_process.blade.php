<div class="row">
    @foreach($driver_work_processes as $work_process)
        <div class="col-md-6 col-lg-3">
            <div class="ic-single-silple wow fadeInUp" data-wow-delay=".0s">
                <div class="ic-icon">
                    <i class="fa {{ $work_process->icon }}" aria-hidden="true"> </i>
                </div>
                <h3>{{ $work_process->title }}</h3>
                <p>{{ $work_process->details }}</p>
            </div>
        </div>
    @endforeach
</div>
