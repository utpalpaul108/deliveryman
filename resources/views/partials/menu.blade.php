@inject('request', 'Illuminate\Http\Request')

<nav class="navbar navbar-expand-lg ic-navbar">
    <a class="navbar-brand" href="/"><img src="/storage/{{ setting('logo') }}" alt=""></a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="sr-only">Toggle navigation</span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent-two">

        @if(Auth::check())
            <ul class="navbar-nav ml-auto ic-navbar-nav nav-3" >
                <li class="nav-item ic-profile">
                    <a class="nav-link" href="#"> <img class="img-fluid" src="@if(!is_null(Auth::user()->profile_image)){{ '/storage/' .Auth::user()->profile_image }} @else {{ '/images/home/after-login.png' }} @endif" alt="Profile Image" style="max-width: 40px"> {{ Auth::user()->name }}</a>
                    <ul class="ic-dropdown-menu">
                        <li><a class="dropdown-item" href="{{ action('UserController@profile') }}">Profile </a></li>
                        <li><a class="dropdown-item" href="{{ action('UserController@notifications') }}">Notification <span class="ic-notification-icon">1</span></a></li>
                        @if(Auth::user()->user_type == 'driver')
                            {{--        For Drivers            --}}
                            <li><a class="dropdown-item" href="{{ action('DeliveryController@assign_works') }}">Assign Works</a></li>
                            <li><a class="dropdown-item" href="{{ action('DeliveryController@index') }}">My Deliveries</a></li>
                            {{--       For Drivers End         --}}
                        @endif
                        @if(Auth::user()->user_type == 'customer')
                            {{--          For Customer             --}}
                            <li><a class="dropdown-item" href="{{ action('OrderController@create') }}">Create New Order</a></li>
                            <li><a class="dropdown-item" href="{{ action('UserController@schedule_request') }}">Schedule Request</a></li>
                            <li><a class="dropdown-item" href="{{ action('OrderController@index') }}">My orders</a></li>
                            {{--       For Customer End         --}}
                        @endif
                        <li><a class="dropdown-item" href="{{ action('UserController@logout') }}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        @else
            <ul class="navbar-nav ml-auto ic-navbar-nav nav-2" >
                <li class="nav-item ic-log-reg">
                    <a class="nav-link ic-register" href="#" data-toggle="modal" data-target="#register"><span>Login / Register</span> <i class="fa fa-user-circle" aria-hidden="true"></i></a>
                </li>
            </ul>
        @endif
    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto ic-navbar-nav nav-1">
            @foreach($items as $item)
                <li class="nav-item {{ request()->is($item->url) ? 'active' : '' }}">
                    <a class="nav-link" href="/{{ $item->url }}">{{ $item->title }} @if(!$item->children->isEmpty())<i class="fa fa-angle-down dropdown-down-angle" aria-hidden="true"></i>@endif</a>
                    @if(!$item->children->isEmpty())
                        @include('partials.menu-items', ['items' => $item->children])
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</nav>
