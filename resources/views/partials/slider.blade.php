

<!-- slider start -->
<div id="ic-slider" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">
        @foreach($sliders as $index=>$slider)
            <div class="carousel-item {{ ($index==0) ? 'active' : '' }}">
                <div class="container">
                    <div class="carousel-caption">
                        <div class="text">
                            <div class="inner">
                                <div class="heading-text">{{ $slider->title ?? '' }} <br/><span>{{ $slider->subtitle ?? '' }}</span></div>
                                <p>{{ $slider->details }}</p>
                                @if(!is_null($slider->details_url))
                                    <div class="ic-banner-btn">
                                        <a href="{{ $slider->details_url['url'] ?? '' }}" class="ic-btn-primary">{{ $slider->details_url['title'] ?? '' }}</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <a class="carousel-control-prev slider-arrow" href="#ic-slider" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next slider-arrow" href="#ic-slider" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- slider end -->
