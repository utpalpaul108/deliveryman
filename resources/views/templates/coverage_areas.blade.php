@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <!-- breadcrumb -->
    <div class="ic-breadcrumb">
        <div class="container">
            <div class="inner">
                <h2>{{ ucwords($page->page_title) }}</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">{{ ucwords($page->page_title) }}</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->

    <div class="coverage-area ic-other-section">
        <!-- top content start -->
        <section class="top-content">
            <div class="container">
                <h2 class="primary-color">{{ $page->contents['coverage_areas_title'] ?? '' }}</h2>
                <h1>{{ $page->contents['coverage_areas_subtitle'] ?? '' }}</h1>
                {!! $page->contents['coverage_areas_details'] ?? '' !!}
                @include('partials.our_coverages')
                <div class="ic-bottom-text">
                    <h2 class="primary-color">{{ $page->contents['coming_soon_title'] }}</h2>
                    <h1>{{ $page->contents['coming_soon_subtitle'] }}</h1>
                    {!! $page->contents['coming_soon_details'] !!}
                </div>
            </div>
        </section>
        <!-- top content end -->
        <!-- bottom content -->
        @include('partials.drive_with_us')
        <!-- bottom content end -->
    </div>
@endsection

@section('script')

@endsection
