<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@show');

// For User Authentication
Route::get('/login', 'UserController@login');
Route::get('/register', 'UserController@register');
Route::get('/logout', 'UserController@logout');
Route::post('/authenticate', 'UserController@authenticate');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// For Admin Authentication
Route::get('/admin-login', 'Admin\UserController@login')->name('login');
Route::get('/admin-logout', 'Admin\UserController@logout');
Route::post('/admin-authenticate', 'Admin\UserController@authenticate');

// For User Confirmation Mail Verification
Route::get('/resend-email','Auth\VerificationController@resend')->name('verification.resend');
Route::get('/verify-email','Auth\VerificationController@show')->name('verification.notice');
Route::get('/verify-email/{id}','Auth\VerificationController@verify')->name('verification.verify');

Route::post('/subscribe', 'SubscriberController@store');
Route::post('/contact-message', 'ContactMessageController@store');


Route::post('/user-registration', 'UserController@store');
Route::get('/profile', 'UserController@profile')->middleware('verified');
Route::get('/edit-profile', 'UserController@edit_profile')->middleware('verified');
Route::post('/update-profile', 'UserController@update_profile')->middleware('verified');
Route::post('/update-subscription-plan', 'SubscriptionPlanController@store')->middleware('verified');
Route::get('/notifications', 'UserController@notifications')->middleware('verified');
Route::get('/schedule-request', 'UserController@schedule_request')->middleware('isCustomer');
Route::get('/select-schedule', 'UserController@select_schedule')->middleware('isCustomer');
Route::post('/set-schedule', 'UserController@set_schedule')->middleware('isCustomer');
Route::get('/my-orders', 'OrderController@index')->middleware('isCustomer');
Route::get('/create-order', 'OrderController@create')->middleware('isCustomer');
Route::post('/store-order', 'OrderController@store')->middleware('isCustomer');
Route::get('/payment-info', 'UserController@payment_info')->middleware('isCustomer');
Route::post('/update-payment-info', 'UserController@update_payment_info')->middleware('isCustomer');
Route::get('/assign-works', 'DeliveryController@assign_works')->middleware('isDriver');
Route::post('/deliver-product', 'DeliveryController@deliver_product')->middleware('isDriver');
Route::get('/my-deliveries', 'DeliveryController@index')->middleware('isDriver');



// For Admin Panel
// ===============================

Route::group(['prefix'=>'/admin', 'middleware'=>'isAdmin', 'namespace'=>'Admin'],function (){

    Route::get('/', 'DashboardController@index');

//    For Dynamic Menu
//    ==================================

    Route::get('/menu', 'MenuController@index');
    Route::post('/menu/create', 'MenuItemController@store');
    Route::get('/menu/delete/{id}', 'MenuItemController@destroy');
    Route::get('/menu/load_data', 'MenuController@loadItems');
    Route::post('/menu/update', 'MenuItemController@update');
    Route::post('/menu/order', 'MenuController@order_item')->name('menus.order');

//    For Dynamic Slider
//    =====================================
    Route::resource('/slider-groups', 'SliderGroupController');
    Route::resource('/sliders', 'SliderController');

//    For Dynamic Page
    Route::post('/load_template','PageController@load_template');
    Route::resource('/pages', 'PageController');

//    For Site Settings
//    =====================================
    Route::get('/site-settings', 'SiteSettingController@index');
    Route::post('/site-settings', 'SiteSettingController@update');

//    For Widget
//    =====================================
    Route::resource('/widgets', 'WidgetController');

//    For Subscribers
//    =====================================
    Route::resource('/subscribers', 'SubscriberController');

//    For Subscription
//    =====================================
    Route::resource('/subscriptions', 'SubscriptionPlanController');

//    For Role & Permissions
//    =====================================
    Route::resource('/modules','ModuleController');
    Route::resource('/user-permissions','UserPermissionController');
    Route::resource('/user-roles','UserRoleController');

//    For Administrators
//    =====================================
    Route::get('/users-pending','UserController@pending_users');
    Route::get('/approve-user/{id}','UserController@approve_user');
    Route::resource('/administrators','UserController');

//    For Customers
//    =====================================
    Route::resource('/customers','CustomerController');

//    For Drivers
//    =====================================
    Route::resource('/drivers','DriverController');
    Route::get('/approve-drivers/{id}','PendingDriverController@approve');
    Route::resource('/pending-drivers','PendingDriverController');

//    Product Delivery
//    =====================================
    Route::get('/delivery-requests','ProductDeliveryController@delivery_requests');
    Route::post('/product-received/{id}','ProductDeliveryController@receive_product');
    Route::get('/received-products','ProductDeliveryController@received_products');
    Route::get('/scheduled-products','ProductDeliveryController@scheduled_products');
    Route::get('/get-drivers/{id}','ProductDeliveryController@get_drivers');
    Route::post('/assign-driver','ProductDeliveryController@assign_driver');
    Route::get('/assigned-products','ProductDeliveryController@assigned_products');
    Route::get('/delivered-products','ProductDeliveryController@delivered_products');

//    FAQ
//    =====================================
    Route::resource('/faq-groups','FAQGroupController');
    Route::resource('/faqs','FAQController');

//    Coverage Area
//    =====================================
    Route::resource('/coverage-areas','CoverageAreaController');

//    Testimonials
//    =====================================
    Route::resource('/testimonials','TestimonialController');

//    Team Members
//    =====================================
    Route::post('/order-members','TeamMemberController@order_member');
    Route::resource('/team-members','TeamMemberController');

//    Media Coverage
//    =====================================
    Route::resource('/media-coverages','MediaPublicationController');

//    Driver Work Process
//    =====================================
    Route::post('/order-driver-work-process','DriverWorkProcessController@manage_order');
    Route::resource('/driver-work-process','DriverWorkProcessController');

//    Driver Benefits
//    =====================================
    Route::post('/order-driver-benefits','DriverBenefitController@manage_order');
    Route::resource('/driver-benefits','DriverBenefitController');

});

// For Viw Pages
// ===========================

Route::get('/{slug}','PageController@show')->name('page.show');
