<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public function faq_group(){
        return $this->belongsTo('App\FaqGroup');
    }

    protected $fillable=['faq_group_id','title','details'];
}
