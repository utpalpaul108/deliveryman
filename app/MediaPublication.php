<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaPublication extends Model
{
    protected $fillable=['media_name','media_logo'];
}
