<?php

namespace App\Http\Controllers\Admin;

use App\SiteSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SiteSettingController extends Controller
{
    public function index(){
        return view('admin.site_setting.index');
    }

    public function update(Request $request){
        $allData=$request->except('_token','logo');
        foreach ($allData as $key=>$data){
            if ($key=='social_icons' || $key=='special_offer'){
                $data=json_encode($data);
            }
            SiteSetting::updateOrCreate(['key'=>$key],['value'=>$data]);
        }

        if ($request->hasFile('logo')){
            $path=$request->file('logo')->store('images');
            $image = Image::make(Storage::get($path))->resize(null, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->encode();
            Storage::put($path, $image);
            SiteSetting::updateOrCreate(['key'=>'logo'],['value'=>$path]);
        }

        if ($request->hasFile('login_image_01')){
            $path=$request->file('login_image_01')->store('images');
            $image = Image::make(Storage::get($path))->fit(570, 500)->encode();
            Storage::put($path, $image);
            SiteSetting::updateOrCreate(['key'=>'login_image_01'],['value'=>$path]);
        }

        flash('Site Settings Updated Successfully');
        return redirect()->back();
    }
}
