<?php

namespace App\Http\Controllers\Admin;

use App\CustomerOrder;
use App\Notifications\AssignDriver;
use App\Notifications\ReceiveProduct;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Validator;

class ProductDeliveryController extends Controller
{
    public function delivery_requests(){
        $delivery_requests=CustomerOrder::with('customer')->where('status','ordered')->get();
        return view('admin.product_delivery.delivery_request',compact('delivery_requests'));
    }

    public function receive_product(Request $request, $id){
        $delivery_request=CustomerOrder::findOrFail($id);
        $delivery_request->status='received';
        $delivery_request->receiving_date=Carbon::now();
        $delivery_request->received_by=Auth::id();
        $delivery_request->save();
        $user=User::findOrFail($delivery_request->customer_id);
        $message['action']='/provide-schedule/'.$id;
        $message['message']='Your product is received';
        Notification::send($user, new ReceiveProduct($message));

        flash('Product received successfully');
        return redirect()->action('Admin\ProductDeliveryController@delivery_requests');
    }

    public function received_products(){
        $received_products=CustomerOrder::where('status','received')->get();
        return view('admin.product_delivery.received_products',compact('received_products'));
    }

    public function scheduled_products(){
        $scheduled_products=CustomerOrder::where('status','scheduled')->get();
        return view('admin.product_delivery.scheduled_products',compact('scheduled_products'));
    }

    public function get_drivers($id){
        $product_order=CustomerOrder::findOrFail($id);
        $drivers=User::where([['user_type','driver'],['status',1]])->whereTime('working_schedule_from', '>=', $product_order->schedule_time_from)
            ->OrWhereTime('working_schedule_from', '<=', $product_order->schedule_time_to)->get();
        return view('admin.product_delivery.driver_list',compact('drivers'));
    }

    public function assign_driver(Request $request){
        $validation=Validator::make($request->all(), [
            'driver_id' => 'exists:users,id'
        ]);

        if ($validation->fails()){
            return redirect()->action('Admin\ProductDeliveryController@scheduled_products')->withErrors($validation);
        }

        $driver=User::findOrFail($request->driver_id);
        $product_order=CustomerOrder::findOrFail($request->order_id);
        $product_order->driver_id=$request->driver_id;
        $product_order->driver_assigned_by=Auth::id();
        $product_order->status='assigned';
        $product_order->save();
        flash('Driver assigned successfully');

        $message['action']='/assigned-works';
        $message['message']='New delivery assigned';
        Notification::send($driver, new AssignDriver($message));

        return redirect()->action('Admin\ProductDeliveryController@scheduled_products');
    }

    public function assigned_products(){
        $assigned_products=CustomerOrder::where('status','assigned')->get();
        return view('admin.product_delivery.assigned_products',compact('assigned_products'));
    }

    public function delivered_products(){
        $delivered_products=CustomerOrder::where('status','delivered')->get();
        return view('admin.product_delivery.delivered_products',compact('delivered_products'));
    }
}
