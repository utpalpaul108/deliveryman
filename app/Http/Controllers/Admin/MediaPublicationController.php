<?php

namespace App\Http\Controllers\Admin;

use App\MediaPublication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MediaPublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media_publications=MediaPublication::all();
        return view('admin.media_publication.index',compact('media_publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.media_publication.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'media_name'=>'required'
        ]);
        $allData=$request->all();
        if ($request->hasFile('media_logo')){
            $path=$request->file('media_logo')->store('images');
            $image = Image::make(Storage::get($path))->resize(172, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode();
            Storage::put($path, $image);
            $allData['media_logo']=$path;
        }
        MediaPublication::create($allData);
        flash('Media publication created successfully');
        return redirect()->action('Admin\MediaPublicationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $media_publication=MediaPublication::findOrFail($id);
        return view('admin.media_publication.edit',compact('media_publication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'media_name'=>'required'
        ]);
        $media_publication=MediaPublication::findOrFail($id);
        if ($request->hasFile('media_logo')){
            Storage::delete($media_publication->media_logo);
            $path=$request->file('media_logo')->store('images');
            $image = Image::make(Storage::get($path))->resize(172, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode();
            Storage::put($path, $image);
            $media_publication->media_logo=$path;
        }
        $media_publication->media_name=$request->media_name;
        $media_publication->save();
        flash('Media publication updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media_publication=MediaPublication::findOrFail($id);
        Storage::delete($media_publication->media_logo);
        MediaPublication::destroy($id);
        flash('Media publication deleted successfully');
        return redirect()->action('Admin\MediaPublicationController@index');
    }
}
