<?php

namespace App\Http\Controllers\Admin;

use App\DriverBenefit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverBenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $driver_benefits=DriverBenefit::orderBy('order')->get();
        return view('admin.driver_benefit.index',compact('driver_benefits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.driver_benefit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'details'=>'required',
            'icon'=>'required',
        ]);

        DriverBenefit::create($request->all());
        flash('New driver benefit added successfully');
        return redirect()->action('Admin\DriverBenefitController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver_benefit=DriverBenefit::findOrFail($id);
        return view('admin.driver_benefit.edit',compact('driver_benefit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver_benefit=DriverBenefit::findOrFail($id);
        $driver_benefit->title=$request->title;
        $driver_benefit->details=$request->details;
        $driver_benefit->icon=$request->icon;
        $driver_benefit->save();
        flash('Driver benefit updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DriverBenefit::destroy($id);
        flash('Driver benefit deleted successfully');
        return redirect()->action('Admin\DriverBenefitController@index');
    }

    public function manage_order(Request $request)
    {
        $driver_benefits = json_decode($request->driver_benefits);
        foreach ($driver_benefits as $index => $driverBenefit) {
            $benefit = DriverBenefit::findOrFail($driverBenefit->id);
            $benefit->order = $index + 1;
            $benefit->save();
        }
    }
}
