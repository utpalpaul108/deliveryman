<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Validator;

class PendingDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers=User::where([['user_type','driver'],['status',2]])->get();
        return view('admin.drivers.pending.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver=User::findOrFail($id);
        return view('admin.drivers.pending.edit',compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'name' => 'required',
            'address' => 'required',
            'phone_no' => 'required',
            'working_schedule_from' => 'required|date_format:H:i',
            'working_schedule_to' => 'required|date_format:H:i',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $driver=User::findOrFail($id);
        $driver->name=$request->name;
        $driver->email=$request->email;
        $driver->address=$request->address;
        $driver->phone_no=$request->phone_no;
        $driver->working_schedule_from=$request->working_schedule_from;
        $driver->working_schedule_to=$request->working_schedule_to;
        if ($request->hasFile('profile_image')){
            Storage::delete($driver->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $driver->profile_image=$path;
        }

        if (isset($request->password)){
            $driver->password=bcrypt($request->password);
        }
        $driver->save();
        flash('Driver information updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->forceDelete();
        flash('Pending driver deleted successfully');
        return redirect()->action('Admin\PendingDriverController@index');
    }

    public function approve($id){
        $driver=User::findOrFail($id);
        $driver->status=1;
        $driver->save();
        flash('Driver approved successfully');
        return redirect()->action('Admin\PendingDriverController@index');
    }
}
